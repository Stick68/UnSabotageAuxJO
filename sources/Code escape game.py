import pygame
import time

# Zone de jeu
pygame.init()  # Initialisation de Pygame
fps = pygame.time.Clock()  # Initialise l'horloge
fps.tick(60)  # Limite le nombre de frames par seconde à 60
Largeur = 800
Hauteur = 600
Zone_Jeu = pygame.display.set_mode((Largeur, Hauteur))  # Crée la fenêtre de jeu
pygame.display.set_caption("Un saboteur aux J-O")  # Définit le titre de la fenêtre de jeu

# Titre + fond + Ecran de fin
titre = pygame.image.load('Titre.png')  # Charge l'image du titre
titre = pygame.transform.scale(titre, (800, 600))  # Redimensionne l'image du titre
titre = titre.convert()  # Convertit l'image du titre pour une utilisation optimale
fond = pygame.image.load('Le_Sol.png')  # Charge l'image du fond
fond = pygame.transform.scale(fond, (800, 600))  # Redimensionne l'image du fond
fond = fond.convert()  # Convertit l'image du fond pour une utilisation optimale
fond2 = pygame.image.load('Fond_Final.png')  # Charge l'image de fond final
fond2 = pygame.transform.scale(fond2, (800, 600))  # Redimensionne l'image de fond final
fond2 = fond2.convert()  # Convertit l'image de fond final pour une utilisation optimale
noir = (0, 0, 0)  # Définit la couleur noire
img_gagne = pygame.image.load('Gagné.png')
img_gagne = pygame.transform.scale(img_gagne,(800,600))
img_perdu = pygame.image.load('Perdu.png')
img_perdu = pygame.transform.scale(img_perdu,(800,600))

# Bouton jouer
jouer = pygame.image.load("Boutton_Jouer.png")  # Charge l'image du bouton jouer
jouer = pygame.transform.scale(jouer, (200, 100))  # Redimensionne l'image du bouton jouer

# Le Personnage
Largueur_personnage = 75
Hauteur_personnage = 75
img_bas = pygame.image.load('MrT_Bas.png')  # Charge l'image du personnage vers le bas
img_bas = pygame.transform.scale(img_bas, (75, 75))  # Redimensionne l'image du personnage vers le bas
img_droite = pygame.image.load('MrT_Droite.png')  # Charge l'image du personnage vers la droite
img_droite = pygame.transform.scale(img_droite, (75, 75))  # Redimensionne l'image du personnage vers la droite
img_gauche = pygame.image.load('MrT_Gauche.png')  # Charge l'image du personnage vers la gauche
img_gauche = pygame.transform.scale(img_gauche, (75, 75))  # Redimensionne l'image du personnage vers la gauche
img_haut = pygame.image.load('MrT_Haut.png')  # Charge l'image du personnage vers le haut
img_haut = pygame.transform.scale(img_haut, (75, 75))  # Redimensionne l'image du personnage vers le haut
img = img_bas  # Définit l'image par défaut du personnage vers le bas

# Le Coupable
img_coupable = pygame.image.load('Coupable.png')  # Charge l'image du coupable
img_coupable = pygame.transform.scale(img_coupable, (200, 250))  # Redimensionne l'image du coupable

# Innocent 1
img_innocent1 = pygame.image.load('Inoccent1.png')  # Charge l'image de l'innocent 1
img_innocent1 = pygame.transform.scale(img_innocent1, (200, 250))  # Redimensionne l'image de l'innocent 1

# Innocent 2
img_innocent2 = pygame.image.load('Inoccent2.png')  # Charge l'image de l'innocent 2
img_innocent2 = pygame.transform.scale(img_innocent2, (200, 250))  # Redimensionne l'image de l'innocent 2

# La Flèche
img_fleche = pygame.image.load('Flèche.png')  # Charge l'image de la flèche
img_fleche = pygame.transform.scale(img_fleche, (100, 100))  # Redimensionne l'image de la flèche

# Le banc
img_banc = pygame.image.load('Banc.png')  # Charge l'image du banc
img_banc = pygame.transform.scale(img_banc, (200, 50))  # Redimensionne l'image du banc

# Casier
img_casier = pygame.image.load('Casiers.png')  # Charge l'image du casier
img_casier = pygame.transform.scale(img_casier, (250, 100))  # Redimensionne l'image du casier

# Le pantalon
img_pantalon = pygame.image.load('Pantalon.png')  # Charge l'image du pantalon
img_pantalon = pygame.transform.scale(img_pantalon, (35, 45))  # Redimensionne l'image du pantalon

# Le maillot
img_maillot = pygame.image.load('Maillot.png')  # Charge l'image du maillot
img_maillot = pygame.transform.scale(img_maillot, (50, 50))  # Redimensionne l'image du maillot

# Les chaussures
img_chaussures = pygame.image.load('Chaussures.png')  # Charge l'image des chaussures
img_chaussures = pygame.transform.scale(img_chaussures, (40, 20))  # Redimensionne l'image des chaussures

# Le chapeau
img_chapeau = pygame.image.load('Chapeau.png')  # Charge l'image du chapeau
img_chapeau = pygame.transform.scale(img_chapeau, (20, 20))  # Redimensionne l'image du chapeau

# Les portes
img_porte_ferme = pygame.image.load('Porte_Fermée.png')  # Charge l'image de la porte fermée
img_porte_ferme = pygame.transform.scale(img_porte_ferme, (100, 100))  # Redimensionne l'image de la porte fermée
img_porte_ouverte = pygame.image.load('Porte_Ouverte.png')  # Charge l'image de la porte ouverte
img_porte_ouverte = pygame.transform.scale(img_porte_ouverte, (100, 100))  # Redimensionne l'image de la porte ouverte

# La balle de bowling
img_Balle_Bowling = pygame.image.load('Balle_Bowling.png')  # Charge l'image de la balle de bowling
img_Balle_Bowling = pygame.transform.scale(img_Balle_Bowling, (30, 30))  # Redimensionne l'image de la balle de bowling

# La balle de basket
img_Balle_Basket = pygame.image.load('Balle_de_Basket.png')  # Charge l'image de la balle de basket
img_Balle_Basket = pygame.transform.scale(img_Balle_Basket, (30, 30))  # Redimensionne l'image de la balle de basket

# La balle de foot
img_Balle_foot = pygame.image.load('Balle_de_Foot.png')  # Charge l'image de la balle de foot
img_Balle_foot = pygame.transform.scale(img_Balle_foot, (30, 30))  # Redimensionne l'image de la balle de foot

# La balle de golf
img_Balle_golf = pygame.image.load('Balle_de_golf.png')  # Charge l'image de la balle de golf
img_Balle_golf = pygame.transform.scale(img_Balle_golf, (20, 20))  # Redimensionne l'image de la balle de golf

# La table
img_Table = pygame.image.load('Table.png')  # Charge l'image de la table
img_Table = pygame.transform.scale(img_Table, (60, 60))  # Redimensionne l'image de la table

# Le texte
text = """Vous étiez en route vers le stade olympique pour voir les jeux quand vous avez
trouvé la route bloquée par une grande foule. Par chance, vous avez vu une porte
ouverte. Vous avez décidé d'entrer parce que vous étiez super impatient.
Mais après être passé par cette porte, vous avez commencé à regretter, pas
seulement parce que c'était sûrement interdit, mais aussi parce que la porte
s'est fermée derrière vous. Maintenant, vous êtes coincé.

Vous devez vous échapper en trouvant et en interagissant avec des objets qui
n'ont pas de rapport avec les sports proposés au jeux pour vous permettre de
voir les jeux. Pour bouger, utilisez les flèches de votre clavier et pour
interagir avec les objets, approchez-vous d'eux et appuyez sur la barre
d'espace.

En essayant de vous échapper, vous remarquez que quelqu'un a voulu ruiner les
jeux, et les objets que vous trouvez sont des indices sur le saboteur.
À la fin du jeu, vous devrez trouver le coupable, alors faites
attention à quels objets intrus vous voyez. Bonne chance !

APPUYEZ SUR ESPACE POUR CONTINUER."""  # Définit le texte du jeu
text_bowling = """Balle de bowling"""  # Texte spécifique pour la balle de bowling
noir = (0, 0, 0)  # Définit la couleur noire
blanc = (255, 255, 255)  # Définit la couleur blanche

# Le jeux
def Jeux():
    global img, Solution, interraction_table, Solution2, Solution3, Solution3perdu
    Solution3perdu = False
    Solution = False
    interraction_table = False
    Texte_bowling = False
    Solution2 = False
    Solution3 = False
    # Coordonnée Coupable
    x_coupable = 550
    y_coupable = 150
    # Coordonnée innocent 1
    x_inoccent1 = 50
    y_inoccent1 = 150
    # Coordonnée inoccent 2
    x_inoccent2 = 300
    y_inoccent2 = 150
    # Coordonnée flèche
    x_fleche = 100
    y_fleche = 425
    # Coordonnée banc
    x_banc = 500
    y_banc = 300
    # Coordonnée casiers
    x_casiers = 0
    y_casiers = 0
    # Coordonnée pantalon
    x_pantalon = 300
    y_pantalon = 300
    # Coordonnée maillot
    x_maillot = 600
    y_maillot = 75
    # Coordonnée Chaussures
    x_chaussures = 500
    y_chaussures = 500
    # Coordonnée chapeau
    x_chapeau = 100
    y_chapeau = 450
    # Coordonnée de base du perso + mouvement
    x = 350
    y = 275
    y_mouvement = 0
    x_mouvement = 0
    # Coordonnée des portes
    x_porte_ferme = 350
    y_porte_ferme = 0
    x_porte_ouverte = 350
    y_porte_ouverte = 0
    # Coordonnée de la balle de bowling
    x_Balle_Bowling = 200
    y_Balle_Bowling = 200
    # Coordonnée de la balle de basket
    x_Balle_Basket = 400
    y_Balle_Basket = 400
    # Coordonnée de la balle de foot
    x_Balle_foot = 500
    y_Balle_foot = 200
    # Coordonnée de la balle de golf
    x_Balle_golf = 200
    y_Balle_golf = 500
    # Coordonnée de la Poubelle
    #x_Poubelle = 725
    #y_Poubelle = 525
    # Coordonnée de la Table
    x_Table = 50
    y_Table = 50
    # Game over
    game_over = False
    # Numero de la salle (0 est l'intro)
    Nb_Salle = -1
    img = img_bas
    # Initialisation de la variable img
    img = img_bas
    # On demarre le jeu
    while not game_over:
        fps.tick(60)
        # Déplacement + animation de mouvement + interaction
        for event in pygame.event.get():
            if event.type == pygame.QUIT :
                game_over = True
            #detection Click
            if event.type == pygame.MOUSEBUTTONDOWN:# Clic gauche
                if event.button == 1:  # 1 pour le clic gauche de la souris
                    pos_souris = pygame.mouse.get_pos()
                    if 250 < pos_souris[0] < 450 and 450 < pos_souris[1] < 550:
                        Nb_Salle = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:# on appui sur la touche HAUT
                    y_mouvement = -3
                    img = img_haut
            if event.type == pygame.KEYUP:
                y_mouvement = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:# on appui sur la touche BAS
                   y_mouvement = 3
                   img = img_bas
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_DOWN:
                    y_mouvement = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:# on appui sur la touche DROITE
                    x_mouvement = 3
                    img = img_droite
                    if Nb_Salle == 3:
                        x_fleche = x_fleche + 250
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    x_mouvement = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:# on appui sur la touche GAUCHE
                    x_mouvement = -3
                    img = img_gauche
                    if Nb_Salle == 3:
                        x_fleche = x_fleche - 250
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    x_mouvement = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:# on appui sur la touche ESPACE
                    if Nb_Salle == 0:
                        Nb_Salle = 1
                    if Nb_Salle == 1:
                        if abs(x - x_Balle_Bowling) < Largueur_personnage and abs(y - y_Balle_Bowling) < Hauteur_personnage:
                            Solution = True
                    if Nb_Salle == 2:
                        if abs(x - x_chapeau) < Largueur_personnage and abs(y - y_chapeau) < Hauteur_personnage:
                            Solution2 = True
                    if Nb_Salle == 3:
                        if x_fleche == 600:
                            Solution3 = True
                        else:
                            Solution3perdu = True
        # Changement de place du perso
        y = y + y_mouvement
        x = x + x_mouvement
        # Collision des bordures de jeu
        if x < 0:
            x = 0
        elif x > Largeur - Largueur_personnage:
            x = Largeur - Largueur_personnage
        if y < 0:
            y = 0
        elif y > Hauteur - Hauteur_personnage:
            y = Hauteur - Hauteur_personnage
        # Colision des bords de la fleche
        if x_fleche > 600:
            x_fleche = 600
        if x_fleche < 0:
            x_fleche = 100
        # Interraction
        if abs(x - x_Balle_Bowling) < Largueur_personnage and abs(y - y_Balle_Bowling) < Hauteur_personnage:
            Texte_bowling = True
        else:
            Texte_bowling = False

        if abs(x - x_Balle_foot) < Largueur_personnage and abs(y - y_Balle_foot) < Hauteur_personnage:
            Texte_foot = True
        else:
            Texte_foot = False

        if abs(x - x_Balle_Basket) < Largueur_personnage and abs(y - y_Balle_Basket) < Hauteur_personnage:
            Texte_Basket = True
        else:
            Texte_Basket = False

        if abs(x - x_Table) < Largueur_personnage and abs(y - y_Table) < Hauteur_personnage:
            Texte_Table = True
        else:
            Texte_Table = False

        if abs(x - x_Balle_golf) < Largueur_personnage and abs(y - y_Balle_golf) < Hauteur_personnage:
            Texte_golf = True
        else:
            Texte_golf = False

        if abs(x - x_chapeau) < Largueur_personnage and abs(y - y_chapeau) < Hauteur_personnage:
            Texte_Chapeau = True
        else:
            Texte_Chapeau = False

        if abs(x - x_chaussures) < Largueur_personnage and abs(y - y_chaussures) < Hauteur_personnage:
            Texte_Chaussures = True
        else:
            Texte_Chaussures = False

        if abs(x - x_maillot) < Largueur_personnage and abs(y - y_maillot) < Hauteur_personnage:
            Texte_Maillot = True
        else:
            Texte_Maillot = False

        if abs(x - x_pantalon) < Largueur_personnage and abs(y - y_pantalon) < Hauteur_personnage:
            Texte_Pantalon = True
        else:
            Texte_Pantalon = False

        if Nb_Salle < 1:
            x = 350
            y = 275

        #Texte Intro
        police = pygame.font.SysFont('arial',20)
        if Nb_Salle == 0:
            Zone_Jeu.fill(noir)
            bulle = pygame.Rect(40,50,80,50)
            text_x, text_y = bulle.topleft
            for ligne in text.splitlines():
                text_x,text_y = Zone_Jeu.blit(police.render(ligne,1,blanc),(text_x,text_y)).bottomleft

        # Solution salle 1
        if Solution == True and abs(x - x_porte_ouverte) < Largueur_personnage and abs(y - y_porte_ouverte) < Hauteur_personnage:
            Nb_Salle = 2
            #Remetre le joueur a sa place et fermer la porte
            x = 350
            y = 600
            Solution = False
        # Solution salle 2
        if Solution2 == True and abs(x - x_porte_ouverte) < Largueur_personnage and abs(y - y_porte_ouverte) < Hauteur_personnage:
            Nb_Salle = 3
            Solution2 = False
        # Images + fond
        #Ecran Titre
        if Nb_Salle == -1:
            Zone_Jeu.blit(titre, (0, 0))
            Zone_Jeu.blit(jouer, (250, 450))
        # Salle 1
        if Nb_Salle == 1:
            Zone_Jeu.blit(fond, (0,0))
            Zone_Jeu.blit(img_Table,(x_Table, y_Table))
            #Zone_Jeu.blit(img_Poubelle,(x_Poubelle, y_Poubelle))
            Zone_Jeu.blit(img_Balle_golf,(x_Balle_golf, y_Balle_golf))
            Zone_Jeu.blit(img_Balle_foot,(x_Balle_foot, y_Balle_foot))
            Zone_Jeu.blit(img_Balle_Bowling, (x_Balle_Bowling, y_Balle_Bowling))
            Zone_Jeu.blit(img_Balle_Basket,(x_Balle_Basket, y_Balle_Basket))
            Zone_Jeu.blit(img_porte_ferme,(x_porte_ferme,y_porte_ferme))
            # affichage de la porte qui s'ouvre
            if Solution == True :
                Zone_Jeu.blit(img_porte_ouverte,(x_porte_ouverte,y_porte_ouverte))
            # affichage du personnage
            Zone_Jeu.blit(img, (x, y))
            # Texte au dessus des balles
            if Texte_Basket == True:
                police=pygame.font.SysFont('arial',20)
                texte_basket=police.render("Balle de Basket",True,blanc)
                Zone_Jeu.blit(texte_basket,[350,375])
            if Texte_bowling == True:
                police=pygame.font.SysFont('arial',20)
                texte_bowling=police.render("Balle de Bowling",True,blanc)
                Zone_Jeu.blit(texte_bowling,[150,175])
            if Texte_foot == True:
                police=pygame.font.SysFont('arial',20)
                texte_foot=police.render("Balle de Foot",True,blanc)
                Zone_Jeu.blit(texte_foot,[460,175])
            if Texte_golf == True:
                police=pygame.font.SysFont('arial',20)
                texte_golf=police.render("Balle de Golf",True,blanc)
                Zone_Jeu.blit(texte_golf,[150,475])
            if Texte_Table == True:
                police=pygame.font.SysFont('arial',20)
                texte_table=police.render("Aide : appuiez sur ESPACE pour interagir avec les objets",True,blanc)
                Zone_Jeu.blit(texte_table,[25,25])
        #Salle 2
        if Nb_Salle == 2:
            Zone_Jeu.blit(fond, (0,0))
            Zone_Jeu.blit(img_porte_ferme,(x_porte_ferme,y_porte_ferme))
            Zone_Jeu.blit(img_chapeau,(x_chapeau,y_chapeau))
            Zone_Jeu.blit(img_chaussures,(x_chaussures,y_chaussures))
            Zone_Jeu.blit(img_maillot,(x_maillot,y_maillot))
            Zone_Jeu.blit(img_pantalon,(x_pantalon,y_pantalon))
            Zone_Jeu.blit(img_casier,(x_casiers,y_casiers))
            Zone_Jeu.blit(img_banc,(x_banc,y_banc))
            # ouverture de la porte
            if Solution2 == True :
                Zone_Jeu.blit(img_porte_ouverte,(x_porte_ouverte,y_porte_ouverte))
            # affichage du personnage
            Zone_Jeu.blit(img, (x, y))
            # Texte au dessus des objet
            if Texte_Chapeau == True:
                police=pygame.font.SysFont('arial',20)
                texte_chapeau=police.render("Un Chapeau",True,blanc)
                Zone_Jeu.blit(texte_chapeau,[50,425])
            if Texte_Chaussures == True:
                police=pygame.font.SysFont('arial',20)
                texte_chaussures=police.render("Des Chaussures",True,blanc)
                Zone_Jeu.blit(texte_chaussures,[450,475])
            if Texte_Maillot == True:
                police=pygame.font.SysFont('arial',20)
                texte_maillot=police.render("Un maillot",True,blanc)
                Zone_Jeu.blit(texte_maillot,[580,50])
            if Texte_Pantalon == True:
                police=pygame.font.SysFont('arial',20)
                texte_maillot=police.render("Un Pantalon",True,blanc)
                Zone_Jeu.blit(texte_maillot,[275,275])
        # Salle_3
        if Nb_Salle == 3:
            Zone_Jeu.blit(fond2, (0,0))
            Zone_Jeu.blit(img_fleche,(x_fleche,y_fleche))
            Zone_Jeu.blit(img_innocent1,(x_inoccent1,y_inoccent1))
            Zone_Jeu.blit(img_innocent2,(x_inoccent2,y_inoccent2))
            Zone_Jeu.blit(img_coupable,(x_coupable,y_coupable))
            police=pygame.font.SysFont('arial',27)
            texte_salle3=police.render("Choisissez le saboteur. ATTENTION vous n'avez qu'un essaie !",True,blanc)
            Zone_Jeu.blit(texte_salle3,[10,50])
            if Solution3 == True:
                Zone_Jeu.blit(img_gagne,(0,0))
            if Solution3perdu == True:
                Zone_Jeu.blit(img_perdu,(0,0))


        # Rafraichissement
        pygame.display.update()
# On lance le jeu
Jeux()
# Quitte le jeu
pygame.quit()



